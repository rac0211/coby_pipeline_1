#!/bin/bash

 CURRENT_PATH="$( cd "$( dirname "${BASH_SOURCE[0]}" )" && pwd )"
 cd $CURRENT_PATH
  
 JAXY_JAR_NAME="jax-y-swarm.jar"
 
 java -jar $JAXY_JAR_NAME
